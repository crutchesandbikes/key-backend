<?php
/**
 * Created by PhpStorm.
 * User: u.kovalev
 * Date: 20.09.15
 * Time: 23:27
 */

namespace WebAnt\UserBundle\Controller;

use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints;

class UserController extends AbstractController
{

    protected $objectClass = 'WebAnt\UserBundle\Entity\User';

    /*
     *  @Security("true")
     *  @Security("is_authenticated()")
     *  @Security("has_role('ROLE_ROLE')")
     * */

    /**
     * @ApiDoc(
     * description="Получение информации о пользователе",
     * section = "User",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID пользователя"}
     * },
     * statusCodes={
     *         200="Успех",
     *         401="Нет доступа",
     *         404="Объект не найден"
     *     }
     * )
     */
    public function getUserAction($id)
    {
        $this->throwIfUserAccessIsDenied($id);
        return parent::getObjectGroup(parent::getObject($id), 'getUser');
    }

    /**
     * @ApiDoc(
     * description="Получение списка пользователей",
     * section = "User",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     */
    public function getUsersAction(Request $request)
    {
        $search = $request->query->all();

        $qb = parent::createQueryBuilder([
            'search' => $search
        ]);
        $response = parent::getPaginatedList($qb);
        return parent::getObjectGroup($response, 'getUser');
    }

    /**
     * @ApiDoc(
     * description="Добавление нового пользователя",
     * section = "User",
     * parameters={
     *     {"name"="username", "dataType"="string", "required"=true, "description"="Логин пользователя"},
     *     {"name"="email", "dataType"="string", "required"=true, "description"="Email пользователя"},
     *     {"name"="password", "dataType"="string", "required"=true, "description"="Пароль пользователя"},
     *     {"name"="roles", "dataType"="array", "required"=true, "description"="Массив с ролями"},
     * },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     */
    public function postUserAction(Request $request)
    {
        // init block
        $data = $this->checkJson($request);

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        $userManager = $this->container->get('fos_user.user_manager');
        if (isset($data['email'])){
            $data['username'] = $data['email'];
        }

        // TODO: move it to abstract controller
        foreach(['username', 'email', 'password'] as $field){
            if(empty($data[$field])){
                throw new HttpException(400, "$field is missing");
            }
        }

        if (!$this->get('security.context')->isGranted('ROLE_ADMIN') ||
            !$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')
        ) {
            // only simple users can register themselves
            $data['roles'] = ['ROLE_USER'];
        }
        if(!isset($data['roles'])){
            $data['roles'] = ['ROLE_USER'];
        }


        // set block
        try{

            /** @var \WebAnt\UserBundle\Entity\User $user */
            $user = $userManager->createUser();
            $user->setUsername($data['username']);
            $user->setEmail($data['email']);
            $user->setPlainPassword($data['password']);

            /*
             * old comment:
             *  добавить ограничение по клиенту, на выставление ролей
             *  если это мобилка регистрирует то жестко ROLE_USER
             * end-old-comment
             * */
            if (is_array($data['roles'])) {
                $user->setRolesRaw($data['roles']);
            } else {
                $user->setRolesRaw([$data['roles']]);
            }

            $user->setEnabled(true);
        } catch(\Exception $e) {
            $em->getConnection()->rollback();
            throw new HttpException(400, 'Bad request');
        }

        // validate block
        $validator = $this->get('validator');
        $errors    = $validator->validate($user);
        if(count($errors)){
            /** @var \Symfony\Component\Validator\ConstraintViolation $error */
            $error = $errors[0];
            $em->getConnection()->rollback();
            $constraint = $error->getConstraint();
//            $error->getParameters()
            $field = $error->getPropertyPath();
            $value = $error->getInvalidValue();

            if($constraint instanceof UniqueEntity) {
                // fields(multiple) as string(single) comma separated?
                throw new HttpException(409, "$field $value is already used");
            } else if($constraint instanceof Constraints\Email){
                throw new HttpException(400, "$value is invalid email");
            } else {
                $class = end(explode('\\', get_class($constraint)));
                throw new HttpException(400, "$field $value is invalid: type=$class");
            }
        }


        // flush block
        try {
            $userManager->updateUser($user, true);
            $em->getConnection()->commit();
        } catch(\Exception $e) {
            $em->getConnection()->rollback();
            throw new HttpException(400, 'Bad request');
        }

        return parent::getObjectGroup($user, 'getUser');
    }

    /**
     * @ApiDoc(
     * description="Изменение пользователя",
     * section = "User",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID пользователя"}
     * },
     * parameters = {
     *     {"name"="email", "dataType"="string", "required"=true, "description"="Email пользователя"},
     *     {"name"="username", "dataType"="string", "required"=true, "description"="Имя пользователя"},
     *     {"name"="password", "dataType"="string", "required"=false, "description"="Пароль пользователя"},
     *     {"name"="roles", "dataType"="array", "required"=false, "description"="Массив с ролями"},
     * },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     */
    public function putUserAction(Request $request, $id)
    {
        $this->throwIfUserAccessIsDenied($id);
        $isAdmin = $this->get('security.context')->isGranted('ROLE_ADMIN');

        $data = $this->checkJson($request);
        $userManager = $this->container->get('fos_user.user_manager');
        $user = parent::getObject($id);
        if(isset($data['username']) and $data['email'] != '') {
            $user->setUsername($data['username']);
        }
        if(isset($data['email']) and $data['email'] != '') {
            $user->setEmail($data['email']);
        }
        if (isset($data['password'])) {
            $user->setPlainPassword($data['password']);
        }
        if (isset($data['roles']) && $isAdmin) {
            if(is_array($data['roles'])) {
                $user->setRolesRaw($data['roles']);
            } else {
                $user->setRolesRaw([$data['roles']]);
            }
        }

        $userManager->updateUser($user, true);
        return parent::getObjectGroup($user, 'getUser');
    }

    /**
     * @ApiDoc(
     * description="Удаление пользователя",
     * section = "User",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID пользователя"}
     * },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public function deleteUserAction($id)
    {
        $this->throwIfUserAccessIsDenied($id);
        return parent::deleteObject($id);
    }

    /**
     * @ApiDoc(
     * description="Получить текущего пользователя из токена",
     * section = "User",
     * statusCodes={
     *         200="Успех",
     *         404="Пользователь не найден"
     *     }
     * )
     */
    public function getUserCurrentAction(){
        /** @var \WebAnt\UserBundle\Entity\User $user */
        $user = $this->get('security.context')->getToken()->getUser();

        if (!is_object($user)){
            throw new HttpException(403, "Not Authorized");
        }
        return parent::getObjectGroup($user, 'getUser');
    }

    /**
     * check if current user has access to requested user
     *
     * @param $user - requested user(support both object and id)
     * @return bool
     */
    private function checkUserAccess($user){
        if(is_numeric($user)){
            $user = parent::getObject($user);
        }

        $isAdmin = $this->get('security.context')->isGranted('ROLE_ADMIN');
        $currentUser = $this->getUser();

        return $isAdmin || $user === $currentUser;
    }

    /**
     * throw 403 if no access on user
     * @param $user
     */
    private function throwIfUserAccessIsDenied($user){
        if(!$this->checkUserAccess($user)){
            throw new HttpException(403);
        }
    }

}