<?php

namespace WebAnt\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=
 *          @ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)
 *      ),
 *      @ORM\AttributeOverride(name="emailCanonical", column=
 *          @ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true)
 *      ),
 *      @ORM\AttributeOverride(name="username", column=
 *          @ORM\Column(type="string", name="username", length=255, nullable=true, unique=true)
 *      ),
 *      @ORM\AttributeOverride(name="usernameCanonical", column=
 *          @ORM\Column(type="string", name="username_canonical", length=255, nullable=true, unique=true)
 *      )
 * })
 *
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 *
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getUser", "getOrder"})
     */
    protected $id;

    /**
     * @var
     * @Assert\NotNull
     * @Groups({"getUser", "getOrder"})
     */
    protected $username;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\PassBundle\Entity\Pass")
     */
    private $pass;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="WebAnt\PassBundle\Entity\Folder",mappedBy="user")
     */
    private $folder;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="WebAnt\PassBundle\Entity\Storage",mappedBy="user")
     */
    private $storage;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="WebAnt\ServiceBundle\Entity\Service",mappedBy="user")
     */
    private $services;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="WebAnt\PassBundle\Entity\Pass",mappedBy="owner")
     */
    private $myPassword;


    /**
     * @var string
     * @Assert\Email
     * (
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @Groups({"getUser", "getOrder"})
     *
     */
    protected $email;

    /**
     * @var
     * @Exclude
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters long"
     * )
     * @Exclude
     */
    protected $password;

    /**
     * @var string
     * @Exclude
     */
    protected $salt;


    /**
     * @var array
     * @Groups({"getUser"})
     */
    protected $roles;


    public function __construct()
    {
        $this->folder = new ArrayCollection();
        $this->storage = new ArrayCollection();
        $this->pass = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->myPassword = new ArrayCollection();
        parent::__construct();
        // your own logic
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function setRolesRaw(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->roles[] = strtoupper($role);
        }
        $this->roles = array_unique($this->roles);
        $this->roles = array_values($this->roles);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param mixed $storage
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return mixed
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param mixed $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param mixed $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    /**
     * @return mixed
     */
    public function getMyPassword()
    {
        return $this->myPassword;
    }

    /**
     * @param mixed $myPassword
     */
    public function setMyPassword($myPassword)
    {
        $this->myPassword = $myPassword;
    }




}
