<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Controller;


use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\ServiceBundle\Entity\Service;
use FOS\RestBundle\Controller\Annotations as FosRoute;

class QueueController extends AbstractController
{
    protected $singleGroup = ['getQueue','getCommand'];
    public function __construct()
    {
        $this->objectClass = 'WebAnt\ServiceBundle\Entity\Queue';
        $this->objectKey = 'id';
    }

    /**
     * @ApiDoc(
     * description="Создание команды",
     * section = "ServiceQueue",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название команды"},
     *     {"name"="command", "dataType"="string", "required"=true, "description"="command"},
     *     {"name"="data", "dataType"="string", "required"=true, "description"="data"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     * @FosRoute\Post("/services/queue")
     */
    public function postServiceQueueAction(Request $request)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $data['service'] = $em->getRepository(Service::class)->find($data['service']);

        return parent::getObjectGroup(parent::createObject($data), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Изменение команды",
     * section = "ServiceQueue",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * parameters = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название команды"},
     *     {"name"="service", "dataType"="string", "required"=true, "description"="Service id"},
     *     {"name"="command", "dataType"="string", "required"=true, "description"="command"},
     *     {"name"="method", "dataType"="string", "required"=true, "description"="method"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     * @FosRoute\Put("/services/queue/{id}")
     */
    public function putServiceQueueAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $data['service'] = $em->getRepository(Service::class)->find($data['service']);


        return parent::getObjectGroup(parent::updateObject($data, $id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение списка команд",
     * section = "ServiceQueue",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     * @FosRoute\Get("/services/queue")
     */
    public function getServicesQueuesAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder(['search' => $search]);
        return parent::getObjectGroup(parent::getPaginatedList($qb), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о команде",
     * section = "ServiceQueue",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Get("/services/queue/{id}")
     */
    public function getServicesQueueAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление команды",
     * section = "ServiceQueue",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Service"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/services/queue/{id}")
     */
    public function deleteServiceQueueAction($id)
    {
        return parent::deleteObject($id);
    }
}
