<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Controller;


use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ServiceController extends AbstractController
{
    public function __construct()
    {
        $this->objectClass = 'WebAnt\ServiceBundle\Entity\Service';
        $this->objectKey = 'id';
    }

    /**
     * @ApiDoc(
     * description="Создание Service",
     * section = "Service",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Service"},
     *     {"name"="url", "dataType"="string", "required"=true, "description"="url Service"},
     *     {"name"="login", "dataType"="string", "required"=true, "description"="login Service"},
     *     {"name"="pass", "dataType"="string", "required"=true, "description"="pass Service"},
     *     {"name"="token", "dataType"="string", "required"=true, "description"="token Service"},
     *     {"name"="token_name", "dataType"="string", "required"=true, "description"="наименование token Service"},
     *     {"name"="token_bool", "dataType"="string", "required"=true, "description"="использовать token"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     */
    public function postServiceAction(Request $request)
    {
        $data = $this->checkJson($request);

        $data['user'] = $this->getUser();

        return parent::getObjectGroup(parent::createObject($data), ['getService','getCommand']);
    }

    /**
     * @ApiDoc(
     * description="Изменение Service",
     * section = "Service",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * parameters = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Service"},
     *     {"name"="url", "dataType"="string", "required"=true, "description"="url Service"},
     *     {"name"="login", "dataType"="string", "required"=true, "description"="login Service"},
     *     {"name"="pass", "dataType"="string", "required"=true, "description"="pass Service"},
     *     {"name"="token", "dataType"="string", "required"=true, "description"="token Service"},
     *     {"name"="token_name", "dataType"="string", "required"=true, "description"="наименование token Service"},
     *     {"name"="token_bool", "dataType"="string", "required"=true, "description"="использовать token"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     */
    public function putServiceAction(Request $request, $id)
    {
        $data = $this->checkJson($request);

        return parent::getObjectGroup(parent::updateObject($data, $id), ['getService','getCommand']);
    }

    /**
     * @ApiDoc(
     * description="Получение списка Servicess",
     * section = "Service",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     */
    public function getServicesAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder(['search' => $search]);
        $qb->andWhere('x.user = :user');
        $qb->setParameter('user',$this->getUser());
        return parent::getObjectGroup(parent::getPaginatedList($qb), ['getService','getCommand']);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о Service",
     * section = "Service",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     */
    public function getServiceAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), ['getService','getCommand']);
    }

    /**
     * @ApiDoc(
     * description="Удаление Service",
     * section = "Service",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Service"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public function deleteServiceAction($id)
    {
        return parent::deleteObject($id);
    }
}
