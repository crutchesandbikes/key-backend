<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Controller;


use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\ServiceBundle\Entity\Command;
use WebAnt\ServiceBundle\Entity\Param;
use WebAnt\ServiceBundle\Entity\Queue;
use WebAnt\ServiceBundle\Entity\Requirements;
use WebAnt\ServiceBundle\Entity\Service;
use FOS\RestBundle\Controller\Annotations as FosRoute;

class CommandController extends AbstractController
{
    protected $singleGroup = ['getObjectService', 'getCommand', 'getParam', 'getRequirement'];

    public function __construct()
    {
        $this->objectClass = 'WebAnt\ServiceBundle\Entity\Command';
        $this->objectKey = 'id';
    }


    /**
     * @ApiDoc(
     * description="выполнение команды",
     * section = "ServiceCommand",
     * parameters={
     *     {"name"="command", "dataType"="string", "required"=true, "description"="command"},
     *     {"name"="params", "dataType"="array", "required"=true, "description"="params"},
     *     {"name"="url", "dataType"="array", "required"=true, "description"="url parameters"},
     *
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     * @FosRoute\Post("/service/commands/run")
     */
    public function postServiceCommandRunAction(Request $request)
    {
        $this->objectClass = Queue::class;
        $data = $this->checkJson($request);

        $em = $this->getDoctrine()->getManager();
        /** @var Service $service */
        /** @var Command $command */

        $command = $em->getRepository(Command::class)->find($data['command']);
        $dataQueue["command"] = $command;
        if (isset($data['params'])) {
            $dataQueue["params"] = vsprintf($command->getTemplate(), $data['params']);
        }
        if (isset($data['url'])) {
            $dataQueue["url"] = vsprintf($command->getCommand(), $data['url']);
        }

        return parent::getObjectGroup(parent::createObject($dataQueue), ['getCommand', 'getQueue']);
    }


    /**
     * @ApiDoc(
     * description="Создание команды",
     * section = "ServiceCommand",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название команды"},
     *     {"name"="service", "dataType"="string", "required"=true, "description"="Service id"},
     *     {"name"="method", "dataType"="string", "required"=true, "description"="method"},
     *     {"name"="method_used", "dataType"="string", "required"=true, "description"="method"},
     *     {"name"="params_body", "dataType"="array", "required"=true, "description"="params"},
     *     {"name"="params_url", "dataType"="array", "required"=true, "description"="params"},
     *     {"name"="template_body", "dataType"="string", "required"=true, "description"="printf template"},
     *     {"name"="template_url", "dataType"="string", "required"=true, "description"="printf template"},
     *
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     * @FosRoute\Post("/service/commands")
     */
    public function postServiceCommandAction(Request $request)
    {
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        if (isset($data['params_body'])) {
            $paramsBody = $data['params_body'];
            unset($data['params_body']);
        }
        if (isset($data['params_url'])) {
            $paramsUrl = $data['params_url'];
            unset($data['params_url']);
        }
        $data['service'] = $em->getRepository(Service::class)->find($data['service']);
        /** @var Command $command */
        $command = parent::createObject($data);

        if (isset($paramsBody)) {
            foreach ($paramsBody as $param) {
                $newParam = $db_service->createObject(Param::class, ["body"=>$command,"title" => $param]);
                $command->getParamsBody()->add($newParam);
            }
        }
        if (isset($paramsUrl)) {
            foreach ($paramsUrl as $param) {
                $newParam = $db_service->createObject(Param::class, ["url"=>$command,"title" => $param]);
                $command->getParamsUrl()->add($newParam);
            }
        }
        return parent::getObjectGroup($command, $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Изменение команды",
     * section = "ServiceCommand",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * parameters = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название команды"},
     *     {"name"="service", "dataType"="string", "required"=true, "description"="Service id"},
     *     {"name"="command", "dataType"="string", "required"=true, "description"="command"},
     *     {"name"="method", "dataType"="string", "required"=true, "description"="method"},
     *     {"name"="method_used", "dataType"="string", "required"=true, "description"="method"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     * @FosRoute\Put("/service/commands/{id}")
     */
    public
    function putServiceCommandAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $data['service'] = $em->getRepository(Service::class)->find($data['service']);


        return parent::getObjectGroup(parent::updateObject($data, $id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение списка команд",
     * section = "ServiceCommand",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     * @FosRoute\Get("/service/commands")
     */
    public
    function getServicesCommandAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder(['search' => $search]);
        return parent::getObjectGroup(parent::getPaginatedList($qb), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о команде",
     * section = "ServiceCommand",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Get("/service/commands/{id}")
     */
    public
    function getServiceCommandAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление команды",
     * section = "ServiceCommand",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Service"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/service/commands/{id}")
     */
    public
    function deleteServiceCommandAction($id)
    {

        return parent::deleteObject($id);
    }
}
