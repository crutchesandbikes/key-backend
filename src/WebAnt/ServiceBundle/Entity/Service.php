<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Service
{

    public function __construct()
    {
        $this->dateCreate = new \DateTime();
        $this->commands = new ArrayCollection();
        $this->tokenBool = false;
        $this->password = new ArrayCollection();
        $this->queue = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getService","getObjectService"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     * @Groups({"getService","getObjectService"})
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     * @Groups({"getService","getObjectService"})
     */
    protected $url;
    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", nullable=true)
     * @Groups({"getService"})
     */
    protected $login;
    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", nullable=true)
     * @Groups({"getService"})
     */
    protected $pass;
    /**
     * @var string
     * @ORM\Column(name="token", type="string", nullable=true)
     * @Groups({"getService"})
     */
    protected $token;
    /**
     * @var string
     * @ORM\Column(name="tokenName", type="string", nullable=true)
     * @Groups({"getService"})
     */
    protected $tokenName;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Command",mappedBy="service")
     * @Groups({"getService"})
     */
    private $commands;
    /**
     * @var
     * @ORM\Column(name="token_bool",type="boolean")
     * @Groups({"getService"})
     */
    private $tokenBool;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\PassBundle\Entity\Pass",inversedBy="service")
     */
    private $password;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Queue",mappedBy="service")
     */
    private $queue;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="WebAnt\UserBundle\Entity\User",inversedBy="services")
     */
    private $user;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getTokenname()
    {
        return $this->tokenName;
    }

    /**
     * @param string $tokenName
     */
    public function setTokenname($tokenName)
    {
        $this->tokenName = $tokenName;
    }

    /**
     * @return mixed
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @param mixed $commands
     */
    public function setCommands($commands)
    {
        $this->commands = $commands;
    }

    /**
     * @return mixed
     */
    public function getTokenBool()
    {
        return $this->tokenBool;
    }

    /**
     * @param mixed $tokenBool
     */
    public function setTokenBool($tokenBool)
    {
        $this->tokenBool = $tokenBool;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @param mixed $queue
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
    }


}