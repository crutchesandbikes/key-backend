<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Command
{

    public function __construct()
    {
        $this->dateCreate = new \DateTime();
        $this->queue = new ArrayCollection();
        $this->paramsBody = new ArrayCollection();
        $this->paramsUrl = new ArrayCollection();
        $this->passParams = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getCommand","getObjCommand"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;
    /**
     * @var
     * @ORM\Column(name="name",type="string")
     * @Groups({"getCommand"})
     */
    protected $name;


    /**
     * @var
     * @Groups({"getCommand"})
     * @ORM\Column(name="method",type="string")
     *
     */
    protected $method;
    /**
     * @var
     * @Groups({"getCommand"})
     * @ORM\Column(name="method_used",type="string",nullable=true)
     */
    private $methodUsed;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Queue",mappedBy="command")
     */
    private $queue;
    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Service",inversedBy="commands")
     * @ORM\JoinColumn(name="service")
     * @Groups({"getCommand"})
     */
    private $service;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Param",mappedBy="url")
     * @Groups({"getCommand"})
     */
    private $paramsUrl;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Param",mappedBy="body")
     * @Groups({"getCommand"})
     */
    private $paramsBody;

    /**
     * @var
     * @ORM\Column(name="temlate_url",type="text")
     * @Groups({"getCommand"})
     */
    protected $templateUrl;

    /**
     * @var
     * @ORM\Column(name="template",type="text",nullable=true)
     * @Groups({"getCommand"})
     */
    private $templateBody;
    /**
     * @var
     * @Groups({"getCommand"})
     * @ORM\OneToMany(targetEntity="WebAnt\PassBundle\Entity\PassParam",mappedBy="command")
     */
    private $passParams;


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getMethodUsed()
    {
        return $this->methodUsed;
    }

    /**
     * @param mixed $methodUsed
     */
    public function setMethodUsed($methodUsed)
    {
        $this->methodUsed = $methodUsed;
    }

    /**
     * @return mixed
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @param mixed $queue
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getParamsUrl()
    {
        return $this->paramsUrl;
    }

    /**
     * @param mixed $paramsUrl
     */
    public function setParamsUrl($paramsUrl)
    {
        $this->paramsUrl = $paramsUrl;
    }

    /**
     * @return mixed
     */
    public function getParamsBody()
    {
        return $this->paramsBody;
    }

    /**
     * @param mixed $paramsBody
     */
    public function setParamsBody($paramsBody)
    {
        $this->paramsBody = $paramsBody;
    }

    /**
     * @return mixed
     */
    public function getTemplateUrl()
    {
        return $this->templateUrl;
    }

    /**
     * @param mixed $templateUrl
     */
    public function setTemplateUrl($templateUrl)
    {
        $this->templateUrl = $templateUrl;
    }

    /**
     * @return mixed
     */
    public function getTemplateBody()
    {
        return $this->templateBody;
    }

    /**
     * @param mixed $templateBody
     */
    public function setTemplateBody($templateBody)
    {
        $this->templateBody = $templateBody;
    }

    /**
     * @return mixed
     */
    public function getPassParams()
    {
        return $this->passParams;
    }

    /**
     * @param mixed $passParams
     */
    public function setPassParams($passParams)
    {
        $this->passParams = $passParams;
    }


}