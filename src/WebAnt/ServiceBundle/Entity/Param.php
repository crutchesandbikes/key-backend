<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Param {

    public function __construct()
    {
        $this->dateCreate = new \DateTime();
    }
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getParam"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;
    /**
     * @var string
     * @ORM\Column(name="title",type="string")
     * @Groups({"getParam"})
     */
    private $title;
    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Command",inversedBy="paramsUrl")
     * @ORM\JoinColumn(name="url",nullable=true,onDelete="CASCADE")
     * @Groups({"getParam"})
     */
    protected $url;
    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Command",inversedBy="paramsBody")
     * @ORM\JoinColumn(name="body",nullable=true,onDelete="CASCADE")
     * @Groups({"getParam"})
     */
    protected $body;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }




}