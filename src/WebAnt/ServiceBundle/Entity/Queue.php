<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\ServiceBundle\Entity;

use Doctrine\DBAL\Schema\Column;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Queue
{

    public function __construct()
    {
        $this->dateCreate = new \DateTime();
        $this->status = 0;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getQueue"})
     */
    protected $id;

    /**
     * @var /DateTime
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;
    /**
     * @var
     * @ORM\Column(name="body",type="text",nullable=true)
     * @Groups({"getQueue"})
     */
    protected $body;
    /**
     * @var
     * @ORM\Column(name="url",type="text",nullable=true)
     * @Groups({"getQueue"})
     */
    protected $url;
    /**
     * @var
     * @ORM\ManyToOne(targetEntity="WebAnt\ServiceBundle\Entity\Service",inversedBy="queue")
     */
    protected $service;
    /**
     * @var integer
     * @ORM\Column(name="status",type="integer")
     * @ORM\JoinColumn(name="status")
     */
    private $status;
    /**
     * @var
     * @ORM\Column(name="method",type="string")
     */
    private $method;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }


    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }




}