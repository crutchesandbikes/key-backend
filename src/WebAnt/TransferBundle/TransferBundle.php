<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\TransferBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TransferBundle extends Bundle
{
}
