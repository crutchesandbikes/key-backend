<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\TransferBundle\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;
use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\TransferBundle\Entity\Transfer;
use WebAnt\UserBundle\Entity\User;

class TransferController extends AbstractController
{
    public function __construct()
    {
        $this->objectClass = 'WebAnt\TransferBundle\Entity\Transfer';
        $this->objectKey = 'id';
    }

    /**
     * @ApiDoc(
     * description="Создание Transfer",
     * section = "Transfer",
     * parameters={
     *     {"name"="user", "dataType"="string", "required"=true, "description"="Название Transfer"},
     *     {"name"="data", "dataType"="string", "required"=true, "description"="Описание Transfer"},
     * },
     * output={
     *       "class"="WebAnt\TransferBundle\Entity\Transfer",
     *       "groups"={"getTransfer"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     */
    public function postTransferAction(Request $request)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $data['user'] = $em->getRepository(User::class)->find($data['user']);
        return parent::getObjectGroup(parent::createObject($data), 'getTransfer');
    }


    /**
     * @ApiDoc(
     * description="Получение списка Transferss",
     * section = "Transfer",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\TransferBundle\Entity\Transfer",
     *       "groups"={"getTransfer"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     */
    public function getTransfersAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder([
            'search' => $search
        ]);

        $qb->leftJoin('x.user', 'p');
        $qb->andWhere('p.id = :id');
        $qb->setParameter('id', $this->getUser()->getId());

        return parent::getObjectGroup(parent::getPaginatedList($qb), 'getTransfer');
    }

    /**
     * @ApiDoc(
     * description="Получение информации о Transfer",
     * section = "Transfer",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Transfer"}
     * },
     * output={
     *       "class"="WebAnt\TransferBundle\Entity\Transfer",
     *       "groups"={"getTransfer"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     */
    public function getTransferAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Transfer $transfer */
        $transfer = $em->getRepository(Transfer::class)->find($id);
        $userId = $transfer->getUser()->getId();

        if ($this->getUser()->getId() == $userId) {
            return parent::getObjectGroup($transfer, 'getTransfer');
        } else {
            return new JsonResponse(["message" => "view disabled", "status" => 403], 403);
        }
    }

    /**
     * @ApiDoc(
     * description="Удаление Transfer",
     * section = "Transfer",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Transfer"}
     * },
     * output={
     *       "class"="WebAnt\TransferBundle\Entity\Transfer",
     *       "groups"={"getTransfer"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public function deleteTransferAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Transfer $transfer */
        $transfer = $em->getRepository(Transfer::class)->find($id);
        $userId = $transfer->getUser()->getId();

        if ($this->getUser()->getId() == $userId) {
            return parent::deleteObject($id);
        } else {
            return new JsonResponse(["message" => "view disabled", "status" => 403], 403);

        }
    }
}
