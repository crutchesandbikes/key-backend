<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\GroupBundle\Entity\Group;
use WebAnt\GroupBundle\Entity\GroupPass;
use WebAnt\PassBundle\Entity\Folder;
use WebAnt\PassBundle\Entity\Pass;
use WebAnt\PassBundle\Entity\Storage;
use WebAnt\PassBundle\Services\PassService;
use WebAnt\ServiceBundle\Entity\Command;
use WebAnt\PassBundle\Entity\PassParam;
use WebAnt\ServiceBundle\Entity\Queue;
use WebAnt\ServiceBundle\Entity\Service;
use WebAnt\TagBundle\Entity\Tag;
use WebAnt\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FosRoute;

/**
 * @property array singleGroup
 */
class PassController extends AbstractController
{


    public function __construct()
    {
        $this->objectClass = 'WebAnt\PassBundle\Entity\Pass';
        $this->objectKey = 'id';
        $this->singleGroup = ['getPass', 'getObjectFolder', 'getGroup', 'getTag', 'getUser', 'getParam','getObjCommand'];
    }


    /**
     * @ApiDoc(
     * description="Создание Pass",
     * section = "Pass",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Pass"},
     *     {"name"="folder", "dataType"="string", "required"=true, "description"="папка Pass"},
     *     {"name"="url", "dataType"="string", "required"=true, "description"="url  Pass"},
     *     {"name"="pass", "dataType"="string", "required"=true, "description"="pass  Pass"},
     *     {"name"="login", "dataType"="string", "required"=true, "description"="login  Pass"},
     *     {"name"="tags", "dataType"="array", "required"=false, "description"="tags Pass"},
     *     {"name"="groups", "dataType"="array", "required"=false, "description"="groups Pass"},
     *     {"name"="description", "dataType"="string", "required"=false, "description"="description Pass"},
     *     {"name"="secretKey", "dataType"="string", "required"=false, "description"="description Pass"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     */
    public function postPassAction(Request $request)
    {
        $data = $this->checkJson($request);
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');
        $em = $this->getDoctrine()->getManager();
        $data['owner'] = $this->getUser();
        if (isset($data['users'])) {
            $users = $data['users'];
            unset($data['users']);
            foreach ($users as $user) {
                $data['users'][] = $em->getRepository(User::class)->find($user);
            }
        }
        $data['users'][] = $this->getUser();

        if (isset($data["folder"])) {
            $folder = $em->getRepository(Folder::class)->find($data["folder"]);
            if (isset($folder)) {
                $data["folder"] = $folder;
            } else {
                $folder = $em->getRepository(Folder::class)->findBy(["name" => $data["folder"]]);
                if (!isset($folder)) {
                    $data["folder"] = $folder;
                } else {
                    $data["folder"] = $db_service->createObject(Folder::class, ["name" => $data["folder"],"user"=>$this->getUser()]);
                }
            }
        }

        if (isset($data['tags'])) {
            $tags = $data['tags'];
            unset($data['tags']);
            foreach ($tags as $tagId) {
                /** @var Tag $tag */
                $tag = $em->getRepository(Tag::class)->find($tagId);
                if (isset($tag)) {
                    $data['tag'][] = $tag;
                } else {
                    $tag = $em->getRepository(Tag::class)->findOneBy(["name" => $tagId]);
                    if (isset($tag)) {
                        $data['tag'][] = $tag;
                    } else {
                        $tag = $db_service->createObject(Tag::class, ["name" => $tagId]);
                        $data['tag'][] = $tag;
                    }
                }
            }
        }
//        if (isset($data['groups'])) {
//            $groups = $data['groups'];
//            unset($data['groups']);
//            foreach ($groups as $groupId) {
//                /** @var GroupPass $group */
//                $group = $em->getRepository(GroupPass::class)->find($groupId);
//                if (isset($group)) {
//                    $data['group'][] = $group;
//                } else {
//                    $group = $em->getRepository(GroupPass::class)->findOneBy(["name" => $groupId]);
//                    if (isset($group)) {
//                        $data['group'][] = $group;
//
//                    } else {
//                        $group = $db_service->createObject(GroupPass::class, ["name" => $groupId]);
//                        $data['group'][] = $group;
//
//                    }
//                }
//            }
//        }
        $searchPass = $em->getRepository(Pass::class)->findOneBy(["name" => $data['name']]);
        if (isset($searchPass)) {
            throw new HttpException(403, "name is used");
        }
        /** @var Pass $pass */

        $pass = parent::createObject($data);

        $db_service->createObject(Storage::class, [
            "user" => $this->getUser(),
            "pass" => $pass,
            "clue" => $data["secretKey"],
            "folder"=>$data['folder']
        ]);
        unset($data['folder']);

        foreach ($data['users'] as $user) {
            /** @var User $user */
            $user->setPass([$pass]);
        }
        if (isset($data['tag'])) {
            foreach ($data['tag'] as $tag) {
                $tag->getPass()->add($pass);
            }
        }
//        if (isset($data['group'])) {
//            foreach ($data['group'] as $group) {
//                $group->setPass($pass);
//            }
//        }
        return parent::getObjectGroup($pass, $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="передача Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Pass"}
     * },
     * parameters = {
     *     {"name"="user", "dataType"="string", "required"=true, "description"="id User"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     * @FosRoute\Post("/passes/send/{id}")
     */
    public function postSendPassAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(["email"=>$data['user']]);

        if (!isset($user)){
            throw new HttpException(404,'not found');
        }
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        $users = $pass->getUsers();
        foreach ($users as $item){
            $usersArray[] = $item;
        }

        if (!in_array($user,$usersArray)) {
            $pass->getUsers()->add($user);
            $em->persist($pass);
            $em->flush();
        }
        return parent::getObjectGroup($pass, $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Изменение Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Pass"}
     * },
     * parameters = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Pass"},
     *     {"name"="url", "dataType"="string", "required"=true, "description"="url  Pass"},
     *     {"name"="pass", "dataType"="string", "required"=true, "description"="pass  Pass"},
     *     {"name"="login", "dataType"="string", "required"=true, "description"="login  Pass"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     */
    public function putPassAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');
        $em = $this->getDoctrine()->getManager();
        $pass = $em->getRepository(Pass::class)->find($id);

        $commandParams = $pass->getCommandParams();
        /** @var PassParam $commandParam */
        foreach ($commandParams as $commandParam) {
            if (strtoupper($commandParam->getCommand()->getMethodUsed())!="PUT")break;

            $paramsUrl  = json_decode($commandParam->getUrl(),true);
            $paramsBody = json_decode($commandParam->getBody(),true);
            $finishUrl = $commandParam->getCommand()->getService()->getUrl();
            $finishUrl .= vsprintf($commandParam->getCommand()->getTemplateUrl(), $paramsUrl);
            $finishBody = vsprintf($commandParam->getCommand()->getTemplateBody(), $paramsBody);
            $db_service->createObject(Queue::class, ["url" => $finishUrl, "body" => $finishBody, "method" => strtoupper($commandParam->getCommand()->getMethod()), "service" => $commandParam->getCommand()->getService()->getId()]);
        }


        $em = $this->getDoctrine()->getManager();

        if (isset($data['groups'])) {
            if ($data['groups'] != []) {
                $groups = $data['groups'];
                unset($data['groups']);
                foreach ($groups as $groupId) {
                    /** @var GroupPass $group */
                    $group = $em->getRepository(GroupPass::class)->find($groupId);
                    if (isset($group)) {
                        $data['group'][] = $group;
                    } else {
                        $group = $em->getRepository(GroupPass::class)->findOneBy(["name" => $groupId]);
                        if (isset($group)) {
                            $data['group'][] = $group;

                        } else {
                            $group = $db_service->createObject(GroupPass::class, ["name" => $groupId]);
                            $data['group'][] = $group;

                        }
                    }
                }
            } else {
                $data['group'] = [];
            }
        }
        if (isset($data['tags'])) {
            if ($data['tags'] != []) {
                $tags = $data['tags'];
                unset($data['tag']);
                foreach ($tags as $tagId) {
                    /** @var Tag $tag */
                    $tag = $em->getRepository(Tag::class)->find($tagId);
                    if (isset($tag)) {
                        $data['tag'][] = $tag;
                    } else {
                        $tag = $em->getRepository(Tag::class)->findOneBy(["name" => $tagId]);
                        if (isset($tag)) {
                            $data['tag'][] = $tag;
                        } else {
                            $tag = $db_service->createObject(Tag::class, ["name" => $tagId]);
                            $data['tag'][] = $tag;
                        }
                    }
                }
            } else {
                $data['tag'] = [];
            }
        }


        return parent::getObjectGroup(parent::updateObject($data, $id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение списка Passess",
     * section = "Pass",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"},
     *         {"name"="secretKey", "dataType"="string"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     */
    public function getPassesAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder([
            'search' => $search
        ]);

        $qb->leftJoin('x.users', 'p');
        $qb->andWhere('p.id = :id');
        $qb->setParameter('id', $this->getUser()->getId());

        return parent::getObjectGroup(parent::getPaginatedList($qb), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Pass"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     *         filters={
     *         {"name"="secretKey", "dataType"="integer"},
     * },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     */
    public function getPassAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        foreach ($pass->getUsers() as $user) {
            /** @var User $user */
            $userId[] = $user->getId();
        }
        if (in_array($this->getUser()->getId(), $userId)) {
            return parent::getObjectGroup($pass, $this->singleGroup);
        } else {
            return new JsonResponse(["message" => "view disabled", "status" => 403], 403);
        }
    }

    /**
     * @ApiDoc(
     * description="Добавление тега Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Pass"}
     * },
     * parameters = {
     *     {"name"="tag", "dataType"="string", "required"=true, "description"="id tag"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Post("/passes/tag/{id}")
     */
    public function postPassTagAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        $tag = $em->getRepository(Tag::class)->find($data['tag']);
        $pass->getTag()->add($tag);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Добавление группы Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Pass"}
     * },
     * parameters = {
     *     {"name"="group", "dataType"="string", "required"=true, "description"="id group"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Post("/passes/group/{id}")
     */
    public function postPassGroupAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        /** @var GroupPass $group */
        $group = $em->getRepository(GroupPass::class)->find($data['group']);
        $pass->getGroup()->add($group);

        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление group из Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Pass"}
     * },
     * parameters = {
     *     {"name"="group", "dataType"="string", "required"=true, "description"="id group"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/passes/group/{id}")
     */
    public function deletePassGroupAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        $group = $em->getRepository(GroupPass::class)->find($data['group']);

        $pass->getGroup()->removeElement($group);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление tag из Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Pass"}
     * },
     * parameters = {
     *     {"name"="tag", "dataType"="string", "required"=true, "description"="id tag"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/passes/tag/{id}")
     */
    public function deleteTagGroupAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        $tag = $em->getRepository(Tag::class)->find($data['tag']);

        $pass->getTag()->removeElement($tag);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление Pass",
     * section = "Pass",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Pass"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public function deletePassAction($id)
    {
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');

        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */


        $pass = $em->getRepository(Pass::class)->find($id);
        if($pass->getOwner() !=$this->getUser()){
            throw new HttpException("409","connot be deleted ");
        }
        $commandParams = $pass->getCommandParams();

        /** @var PassParam $commandParam */
        foreach ($commandParams as $commandParam) {
            if (strtoupper($commandParam->getCommand()->getMethodUsed())!="DELETE")break;

            $paramsUrl  = json_decode($commandParam->getUrl(),true);
            $paramsBody = json_decode($commandParam->getBody(),true);
            $finishUrl  = $commandParam->getCommand()->getService()->getUrl();
            $finishUrl .= vsprintf($commandParam->getCommand()->getTemplateUrl(), $paramsUrl);
            $finishBody = vsprintf($commandParam->getCommand()->getTemplateBody(), $paramsBody);

            $db_service->createObject(Queue::class, ["url" => $finishUrl, "body" => $finishBody, "method" => strtoupper($commandParam->getCommand()->getMethod()), "service" => $commandParam->getCommand()->getService()->getId()]);
        }
        return parent::deleteObject($id);
    }
}
