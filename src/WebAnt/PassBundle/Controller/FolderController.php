<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Controller;


use Symfony\Component\HttpKernel\Exception\HttpException;
use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\GroupBundle\Entity\GroupPass;
use WebAnt\PassBundle\Entity\Folder;
use FOS\RestBundle\Controller\Annotations as FosRoute;
use WebAnt\TagBundle\Entity\Tag;
use WebAnt\UserBundle\Entity\User;

class FolderController extends AbstractController
{
    public function __construct()
    {
        $this->objectClass = 'WebAnt\PassBundle\Entity\Folder';
        $this->objectKey = 'id';
    }

    protected $singleGroup = ['getFolder', 'getPass', 'getTag', 'getGroup','getStorage'];

    /**
     * @ApiDoc(
     * description="Добавление user в  Pass",
     * section = "Folder",
     * parameters={
     *     {"name"="user", "dataType"="integer", "required"=true, "description"="User"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     * @FosRoute\Post("/folders/user/{id}")
     */
    public function postFolderUserAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        /** @var Folder $folder */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($data['user']);
        if (!isset($user)){
            $user = $em->getRepository(User::class)->findOneBy(["email"=>$data['user']]);
        }
        $folder = $em->getRepository(Folder::class)->find($id);
        $folder->getUsers()->add($user);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }


    /**
     * @ApiDoc(
     * description="Удаление Pass",
     * section = "Folder",
     * parameters={
     *     {"name"="user", "dataType"="integer", "required"=true, "description"="User"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     * @FosRoute\Delete("/folders/user/{id}")
     */
    public function deleteFolderUserAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        /** @var Folder $folder */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($data['user']);
        $folder = $em->getRepository(Folder::class)->find($id);
        $folder->getUsers()->removeElement($user);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Добовление Pass",
     * section = "Folder",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Folder"},
     *     {"name"="parent", "dataType"="string", "required"=false, "description"="родитель Folder id"},
     *     {"name"="tags", "dataType"="array", "required"=false, "description"="tags"},
     *     {"name"="groups", "dataType"="array", "required"=false, "description"="groups"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     */
    public function postFolderAction(Request $request)
    {
        $data = $this->checkJson($request);
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');

        $em = $this->getDoctrine()->getManager();
        /** @var Folder $folder */
        $data['users'][] = $this->getUser();
        if (isset($data['folder'])){
            $folder = $em->getRepository(Folder::class)->findOneBy(["name" => $data['name'], "parent" => $data['parent']]);
        }
        if (isset($folder)) {
            throw new  HttpException(403, "name is used");
        }
        if (isset($data['tags'])) {
            $tags = $data['tags'];
            unset($data['tag']);
            foreach ($tags as $tagId) {
                /** @var Tag $tag */
                $tag = $em->getRepository(Tag::class)->find($tagId);
                if (isset($tag)) {
                    $data['tag'][] = $tag;
                } else {
                    $tag = $em->getRepository(Tag::class)->findOneBy(["name" => $tagId]);
                    if (isset($tag)) {
                        $data['tag'][] = $tag;
                    } else {
                        $tag = $db_service->createObject(Tag::class, ["name" => $tagId]);
                        $data['tag'][] = $tag;
                    }
                }
            }
        }
        if (isset($data['groups'])) {
            $groups = $data['groups'];
            unset($data['groups']);
            foreach ($groups as $groupId) {
                /** @var GroupPass $group */
                $group = $em->getRepository(GroupPass::class)->find($groupId);
                if (isset($group)) {
                    $data['group'][] = $group;
                } else {
                    $group = $em->getRepository(GroupPass::class)->findOneBy(["name" => $groupId]);
                    if (isset($group)) {
                        $data['group'][] = $group;

                    } else {
                        $group = $db_service->createObject(GroupPass::class, ["name" => $groupId]);
                        $data['group'][] = $group;

                    }
                }
            }
        }
        $data['user']= $this->getUser();
        /** @var Folder $newFolder */
        $newFolder = parent::createObject($data);

        if (isset($data['tag'])) {
            foreach ($data['tag'] as $tag) {
                $newFolder->getTag()->add($tag);
            }
        }
        if (isset($data['group'])) {
            foreach ($data['group'] as $group) {
                $newFolder->getGroup()->add($group);
            }
        }

        return parent::getObjectGroup($newFolder, $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Изменение Folder",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Folder"}
     * },
     * parameters = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Folder"},
     *     {"name"="parent", "dataType"="string", "required"=false, "description"="родитель Folder id"},
     *     {"name"="tags", "dataType"="array", "required"=false, "description"="tags"},
     *     {"name"="groups", "dataType"="array", "required"=false, "description"="groups"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     */
    public function putFolderAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');
        if (isset($data['parent'])){
            if ($data['parent']==0){
                $data['parent'] = null;
            }
        }
        $em = $this->getDoctrine()->getManager();

        if (isset($data['groups'])) {
            if ($data['groups'] != []) {
                $groups = $data['groups'];
                unset($data['groups']);
                foreach ($groups as $groupId) {
                    /** @var GroupPass $group */
                    $group = $em->getRepository(GroupPass::class)->find($groupId);
                    if (isset($group)) {
                        $data['group'][] = $group;
                    } else {
                        $group = $em->getRepository(GroupPass::class)->findOneBy(["name" => $groupId]);
                        if (isset($group)) {
                            $data['group'][] = $group;

                        } else {
                            $group = $db_service->createObject(GroupPass::class, ["name" => $groupId]);
                            $data['group'][] = $group;

                        }
                    }
                }
            } else {
                $data['group'] = [];
            }
        }
        if (isset($data['tags'])) {
            if ($data['tags'] != []) {
                $tags = $data['tags'];
                unset($data['tag']);
                foreach ($tags as $tagId) {
                    /** @var Tag $tag */
                    $tag = $em->getRepository(Tag::class)->find($tagId);
                    if (isset($tag)) {
                        $data['tag'][] = $tag;
                    } else {
                        $tag = $em->getRepository(Tag::class)->findOneBy(["name" => $tagId]);
                        if (isset($tag)) {
                            $data['tag'][] = $tag;
                        } else {
                            $tag = $db_service->createObject(Tag::class, ["name" => $tagId]);
                            $data['tag'][] = $tag;
                        }
                    }
                }
            } else {
                $data['tag'] = [];
            }
        }

        return parent::getObjectGroup(parent::updateObject($data, $id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение списка Folder",
     * section = "Folder",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     */
    public function getFoldersAction(Request $request)
    {
        $search = $request->query->all();
        if (isset($search['parent'])){
            if ($search['parent']=="null"){
                $parent = true;
                unset($search['parent']);
            }
        }

        $qb = parent::createQueryBuilder(['search' => $search]);
        $qb->andWhere('x.user = :user');
        $qb->setParameter('user', $this->getUser()->getId());
        if (isset($parent)){
            $qb->andWhere('x.parent is NULL');
        }
        return parent::getObjectGroup(parent::getPaginatedList($qb), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о Pass",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Folder"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     */
    public function getFolderAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Добавление тега Pass",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Folder"}
     * },
     * parameters = {
     *     {"name"="tag", "dataType"="string", "required"=true, "description"="id tag"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Post("/folders/tag/{id}")
     */
    public function postFolderTagAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Folder $folder */
        $folder = $em->getRepository(Folder::class)->find($id);
        $tag = $em->getRepository(Tag::class)->find($data['tag']);
        $folder->getTag()->add($tag);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Добавление группы Pass",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Folder"}
     * },
     * parameters = {
     *     {"name"="group", "dataType"="string", "required"=true, "description"="id group"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Post("/folders/group/{id}")
     */
    public function postFolderGroupAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Folder $folder */
        $folder = $em->getRepository(Folder::class)->find($id);
        /** @var GroupPass $group */
        $group = $em->getRepository(GroupPass::class)->find($data['group']);
        $folder->getGroup()->add($group);

        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление group из Folder",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Folder"}
     * },
     * parameters = {
     *     {"name"="group", "dataType"="string", "required"=true, "description"="id group"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/folders/group/{id}")
     */
    public function deleteFolderGroupAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Folder $folder */
        $folder = $em->getRepository(Folder::class)->find($id);
        $group =$em->getRepository(GroupPass::class)->find($data['group']);

        $folder->getGroup()->removeElement($group);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление tag из Folder",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Folder"}
     * },
     * parameters = {
     *     {"name"="tag", "dataType"="string", "required"=true, "description"="id tag"},
     *
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/folders/tag/{id}")
     */
    public function deleteFolderTagAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        /** @var Folder $folder */
        $folder = $em->getRepository(Folder::class)->find($id);
        $group =$em->getRepository(Tag::class)->find($data['tag']);

        $folder->getTag()->removeElement($group);
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление Folder",
     * section = "Folder",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Folder"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Folder",
     *       "groups"={"getFolder"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public function deleteFolderAction($id)
    {
        return parent::deleteObject($id);
    }
}
