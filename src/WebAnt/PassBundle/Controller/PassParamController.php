<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Controller;


use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\PassBundle\Entity\Pass;
use WebAnt\ServiceBundle\Entity\Command;
use WebAnt\ServiceBundle\Entity\Param;
use WebAnt\ServiceBundle\Entity\Queue;
use WebAnt\ServiceBundle\Entity\Requirements;
use WebAnt\ServiceBundle\Entity\Service;
use FOS\RestBundle\Controller\Annotations as FosRoute;

class PassParamController extends AbstractController
{
    protected $singleGroup = ['getObjCommand', 'getObjPass', 'getParam'];

    public function __construct()
    {
        $this->objectClass = 'WebAnt\PassBundle\Entity\PassParam';
        $this->objectKey = 'id';
    }


    /**
     * @ApiDoc(
     * description="добавить параметры к паролю",
     * section = "PassParam",
     * parameters={
     *     {"name"="pass", "dataType"="string", "required"=true, "description"="ID pass"},
     *     {"name"="url", "dataType"="array", "required"=true, "description"="url"},
     *     {"name"="body", "dataType"="array", "required"=true, "description"="url"},
     *     {"name"="command", "dataType"="string", "required"=true, "description"="ID command"},
     *
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     * @FosRoute\Post("/params")
     */
    public function postPassesParamAction(Request $request)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $data['pass'] = $em->getRepository(Pass::class)->find($data['pass']);
        $data['command'] = $em->getRepository(Command::class)->find($data['command']);
        $data['url']=json_encode($data['url']);
        $data['body']=json_encode($data['body']);

        return parent::getObjectGroup(parent::createObject($data), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Изменение параметров к паролю",
     * section = "PassParam",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Service"}
     * },
     * parameters = {
     *     {"name"="pass", "dataType"="string", "required"=true, "description"="ID pass"},
     *     {"name"="url", "dataType"="array", "required"=true, "description"="url"},
     *     {"name"="body", "dataType"="array", "required"=true, "description"="url"},
     *     {"name"="command", "dataType"="string", "required"=true, "description"="ID command"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     * @FosRoute\Put("/params/{id}")
     */
    public
    function putPassesParamAction(Request $request, $id)
    {
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        if(isset($data['pass']))$data['pass'] = $em->getRepository(Pass::class)->find($data['pass']);
        if(isset($data['command']))$data['command'] = $em->getRepository(Command::class)->find($data['command']);
        if(isset($data['url']))$data['url']=json_encode($data['url']);
        if(isset($data['body']))$data['body']=json_encode($data['body']);


        return parent::getObjectGroup(parent::updateObject($data, $id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение списка команд",
     * section = "PassParam",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     * @FosRoute\Get("/params")
     */
    public
    function getPassesParamsAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder(['search' => $search]);
        return parent::getObjectGroup(parent::getPaginatedList($qb), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о команде",
     * section = "PassParam",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID PassParam"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     * @FosRoute\Get("/params/{id}")
     */
    public
    function getPassesParamAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление команды",
     * section = "PassParam",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID PassParam"}
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     * @FosRoute\Delete("/params/{id}")
     */
    public
    function deletePassesParamAction($id)
    {

        return parent::deleteObject($id);
    }
}
