<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;
use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use WebAnt\GroupBundle\Entity\GroupPass;
use WebAnt\PassBundle\Entity\Folder;
use WebAnt\PassBundle\Entity\Pass;
use WebAnt\PassBundle\Entity\Storage;
use WebAnt\PassBundle\Services\PassService;
use WebAnt\TagBundle\Entity\Tag;
use WebAnt\UserBundle\Entity\User;

/**
 * @property array singleGroup
 */
class StorageController extends AbstractController
{


    public function __construct()
    {
        $this->objectClass = 'WebAnt\PassBundle\Entity\Storage';
        $this->objectKey = 'id';
        $this->singleGroup = ['getStorage', 'getObjPass', 'getUser', 'getFolder','getTag'];
    }

    /**
     * @ApiDoc(
     * description="Получение списка Passess",
     * section = "Storage",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"},
     *         {"name"="secretKey", "dataType"="string"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *     }
     * )
     */
    public function getStoragesAction(Request $request)
    {
        $search = $request->query->all();
        $qb = parent::createQueryBuilder([
            'search' => $search
        ]);

        $qb->andWhere('x.user = :id');
        $qb->setParameter('id', $this->getUser()->getId());

        return parent::getObjectGroup(parent::getPaginatedList($qb), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Получение информации о Pass",
     * section = "Storage",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Pass"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     *         filters={
     *         {"name"="secretKey", "dataType"="string"}
     * },
     * statusCodes={
     *         200="Успех",
     *         404="Объект не найден"
     *     }
     * )
     */
    public function getStorageAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Pass $pass */
        $pass = $em->getRepository(Pass::class)->find($id);
        foreach ($pass->getUsers() as $user) {
            /** @var User $user */
            $userId[] = $user->getId();
        }
        if (in_array($this->getUser()->getId(), $userId)) {
            return parent::getObjectGroup($pass, $this->singleGroup);
        } else {
            return new JsonResponse(["message" => "view disabled", "status" => 403], 403);
        }
    }


    /**
     * @ApiDoc(
     * description="Добавление Pass в Storage",
     * section = "Storage",
     * parameters={
     *     {"name"="pass", "dataType"="integer", "required"=true, "description"="id Pass"},
     *     {"name"="clue", "dataType"="integer", "required"=true, "description"="secretKey Pass"},
     *     {"name"="folder", "dataType"="string", "required"=true, "description"="Folder"},
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Pass",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
     */
    public function postStorageAction(Request $request)
    {
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        $data['user'] = $this->getUser();
        $data['pass'] = $em->getRepository(Pass::class)->find($data['pass']);

        if (isset($data["folder"])) {
            $folder = $em->getRepository(Folder::class)->find($data["folder"]);
            if (isset($folder)) {
                $data["folder"] = $folder;
            } else {
                $folder = $em->getRepository(Folder::class)->findBy(["name" => $data["folder"]]);
                if (isset($folder)) {
                    $data["folder"] = $folder;
                } else {
                    $data["folder"] = $db_service->createObject(Folder::class, ["name" => $data["folder"]]);
                }
            }
        }

        return parent::getObjectGroup(parent::createObject($data), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Изменение storage",
     * section = "Storage",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID storage"}
     * },
     * parameters = {
     *     {"name"="pass", "dataType"="integer", "required"=true, "description"="id Pass"},
     *     {"name"="clue", "dataType"="integer", "required"=true, "description"="secretKey Pass"},
     *     {"name"="folder", "dataType"="string", "required"=true, "description"="Folder"},
     * },
     * output={
     *       "class"="WebAnt\ServiceBundle\Entity\Service",
     *       "groups"={"getService"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     *     }
     * )
     */
    public
    function putStorageAction(Request $request, $id)
    {
        /** @var \WebAnt\CoreBundle\Services\DatabaseService $db_service */
        $db_service = $this->get('webant.core.database');
        $data = $this->checkJson($request);
        $em = $this->getDoctrine()->getManager();
        if (isset($data['user'])) $data['user'] = $this->getUser();
        if (isset($data['pass'])) $data['pass'] = $em->getRepository(Pass::class)->find($data['pass']);

        if (isset($data["folder"])) {
            if (is_integer($data['folder'])) {
                if ($data['folder'] == 0) {
                    $data['folder'] = null;
                }else{
                    $data['folder'] = $em->getRepository(Folder::class)->find($data['folder']);
                }
            }
            if (is_string($data['folder'])) {
                $folder = $em->getRepository(Folder::class)->findOneBy(["name" => $data["folder"]]);
                if (isset($folder)) {
                    $data["folder"] = $folder;
                } else {
                    $data["folder"] = $db_service->createObject(Folder::class, ["name" => $data["folder"],"user"=>$this->getUser()]);
                }
            }
        }
        return parent::getObjectGroup(parent::updateObject($data, $id), $this->singleGroup);
    }

    /**
     * @ApiDoc(
     * description="Удаление Pass",
     * section = "Storage",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Storage"}
     * },
     * output={
     *       "class"="WebAnt\PassBundle\Entity\Storage",
     *       "groups"={"getPass"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public
    function deleteStorageAction($id)
    {
        return parent::deleteObject($id);
    }


}
