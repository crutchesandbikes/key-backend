<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;
use WebAnt\GroupBundle\Entity\GroupPass;


/**
 * @ORM\Entity
 * @ORM\Table()
 * @UniqueEntity("name")
 */
class Folder
{

    public function __construct()
    {
        $this->tag = new ArrayCollection();
        $this->group = new ArrayCollection();
        $this->dateCreate = new \DateTime();
        $this->children = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getFolder","getObjectFolder"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     * @Groups({"getFolder","getObjectFolder"})
     */
    protected $name;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Storage",mappedBy="folder")
     * @Groups({"getFolder"})
     */
    private $storage;
    /**
     * @var
     * @ORM\ManyToOne(targetEntity="WebAnt\UserBundle\Entity\User",inversedBy="folder")
     */
    private $user;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\TagBundle\Entity\Tag")
     * @Groups({"getPass"})
     */
    private $tag;
    /**
     * @var
     * @Groups({"getPass"})
     * @ORM\ManyToMany(targetEntity="WebAnt\GroupBundle\Entity\GroupPass")
     */
    private $group;
    /**
     * @var
     * @Groups({"getPass"})
     * @ORM\Column(name="description",type="string",nullable=true)
     */
    private $description;

    /**
     * @var \WebAnt\PassBundle\Entity\Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder",inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_parent",referencedColumnName="id",nullable=true,onDelete="CASCADE")
     *})
     *
     */
    private $parent;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Folder", mappedBy="parent")
     **/
    private $children;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Folder
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Folder $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @VirtualProperty
     * @SerializedName("parent_id")
     * @Groups({"getFolder"})
     */
    public function getObjectParrent()
    {
        if (isset($this->parent)) {
            return $this->parent->id;
        }
    }
    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param mixed $storage
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}