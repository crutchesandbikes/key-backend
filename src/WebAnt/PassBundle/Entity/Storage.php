<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use Symfony\Component\HttpFoundation\Request;
use WebAnt\PassBundle\Services\PassService;

/**
 * @ORM\Entity
 * @ORM\Table()
 * )
 * @UniqueEntity({"user","pass"})
 */
class Storage
{

    public function __construct()
    {
        $this->dateCreate = new \DateTime();
        $this->pass = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getStorage","getObjectFolder"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     * @Groups({"getStorage"})
     */
    private $dateCreate;
    /**
     * @var
     * @ORM\ManyToOne(targetEntity="WebAnt\UserBundle\Entity\User",inversedBy="storage")
     * @ORM\JoinColumn(name="user")
     */
    private $user;
    /**
     * @var ArrayCollection
     * @ORM\ManyToOne(targetEntity="Pass",inversedBy="storage")
     * @ORM\JoinColumn(name="pass",onDelete="CASCADE")
     * @Groups({"getStorage"})
     */
    private $pass;
    /**
     * @var
     * @ORM\Column(name="clue",type="string")
     * @Groups({"getStorage"})
     */
    private $clue;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Folder",inversedBy="pass")
     * @ORM\JoinColumn(name="folder_id")
     * @Groups({"getStorage"})
     */
    protected $folder;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return ArrayCollection
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param ArrayCollection $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getClue()
    {
        return $this->clue;
    }

    /**
     * @param mixed $clue
     */
    public function setClue($clue)
    {
        $this->clue = $clue;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param string $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }


}