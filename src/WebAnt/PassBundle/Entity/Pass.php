<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\PassBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use Symfony\Component\HttpFoundation\Request;
use WebAnt\PassBundle\Services\PassService;


/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Pass
{

    public function __construct()
    {
        $this->group = new ArrayCollection();
        $this->tag = new ArrayCollection();
        $this->dateCreate = new \DateTime();
        $this->users = new ArrayCollection();
        $this->service = new ArrayCollection();
        $this->commandParams = new ArrayCollection();

    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getPass","getObjPass"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;
    /**
     * @var
     * @Groups({"getPass"})
     * @ORM\ManyToMany(targetEntity="WebAnt\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id")
     */
    private $users;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     * @Groups({"getPass","getObjPass"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", nullable=true)
     * @Groups({"getPass","getObjPass"})
     */
    protected $login;
    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", nullable=true)
     * @Groups({"getPass","getObjPass"})
     */
    protected $pass;
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     * @Groups({"getPass","getObjPass"})
     */
    protected $url;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\TagBundle\Entity\Tag")
     * @Groups({"getPass","getObjPass"})
     */
    private $tag;
    /**
     * @var
     * @Groups({"getPass"})
     * @ORM\ManyToMany(targetEntity="WebAnt\GroupBundle\Entity\GroupPass")
     */
    private $group;
    /**
     * @var
     * @Groups({"getPass","getObjPass"})
     * @ORM\Column(name="description",type="string",nullable=true)
     */
    private $description;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Storage",mappedBy="pass")
     */
    private $storage;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\ServiceBundle\Entity\Service",mappedBy="password")
     */
    private $service;

//    /**
//     * @var
//     * @ORM\Column(name="params_body",type="text",nullable=true)
//     * @Groups({"getPass","getObjPass"})
//     */
//    private $paramsBody;
//    /**
//     * @var
//     * @ORM\Column(name="params_url",type="text",nullable=true)
//     * @Groups({"getPass","getObjPass"})
//     */
//    private $paramsUrl;
    /**
     * @var
     * @Groups({"getPass","getObjPass"})
     * @ORM\OneToMany(targetEntity="WebAnt\PassBundle\Entity\PassParam",mappedBy="pass")
     */
    private $commandParams;
    /**
     * @var
     * @Groups({"getPass","getObjPass"})
     * @ORM\ManyToOne(targetEntity="WebAnt\UserBundle\Entity\User",inversedBy="myPassword")
     */
    private $owner;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param string $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param mixed $storage
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getCommandParams()
    {
        return $this->commandParams;
    }

    /**
     * @param mixed $commandParams
     */
    public function setCommandParams($commandParams)
    {
        $this->commandParams = $commandParams;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }




//    /**
//     * @return mixed
//     */
//    public function getParamsBody()
//    {
//        return $this->paramsBody;
//    }
//
//    /**
//     * @param mixed $paramsBody
//     */
//    public function setParamsBody($paramsBody)
//    {
//        $this->paramsBody = $paramsBody;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getParamsUrl()
//    {
//        return $this->paramsUrl;
//    }
//
//    /**
//     * @param mixed $paramsUrl
//     */
//    public function setParamsUrl($paramsUrl)
//    {
//        $this->paramsUrl = $paramsUrl;
//    }


}