<?php

namespace WebAnt\ClientBundle\Tests\Controller;

use WebAnt\CoreBundle\Tests\RestWebTestCase;

use Nelmio\Alice\Fixtures;

class ClientControllerTest extends RestWebTestCase
{

    static private $get_item  = 'get_client';
    static private $get_items = 'get_clients';
    static private $post_item = 'post_clients';
    static private $put_item  = 'put_clients';
    static private $del_item  = 'delete_clients';

    protected $fixtureFiles = ['@ClientBundle/DataFixtures/ORM/client.yml'];
    protected $className    = 'WebAnt\ClientBundle\Entity\Client';


    public function testGetClients()
    {
        $result = $this->getTest(
            self::$get_items
        );
        $this->assertEquals($result->count, count($this->entityValue[$this->className]));
    }

    public function testGetClient()
    {
        $result = $this->getTest(
            self::$get_item,
            [
                "id" => $this->firstEntity->getId()
            ]
        );
        $this->assertEquals($result->name, $this->firstEntity->getName());
    }

    public function testPostClient()
    {
        $std       = new \stdClass();
        $std->name = "1";

        $result = $this->postTest(
            self::$post_item,
            $std
        );
        $this->assertEquals($result->name, $std->name);
    }

    public function testPutClient(){
        $std       = new \stdClass();
        $std->name = "123";

        $result = $this->putTest(
            self::$put_item,
            $std,
            array(
                "id"           => $this->firstEntity->getId()
            )
        );

        $this->assertEquals($result->name, $std->name);
    }


    public function testDeleteClient(){

        $this->delTest(
            self::$del_item,
            array(
                "id"           => $this->firstEntity->getId()
            )
        );

        $result = $this->getTest(self::$get_items);
        $this->assertEquals($result->count, count($this->entityValue[$this->className]) - 1);
    }

}
