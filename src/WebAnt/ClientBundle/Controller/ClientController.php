<?php
/**
 * Created by PhpStorm.
 * User: u.kovalev
 * Date: 10.07.2015
 * Time: 19:31
 */

namespace WebAnt\ClientBundle\Controller;

use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ClientController extends AbstractController {

    public function __construct()
    {
        $this->objectClass = 'WebAnt\ClientBundle\Entity\Client';
    }

    /**
     * @ApiDoc(
     * description="Получение клиента",
     * section = "Clients",
     * requirements = {
     *     {"name"="id", "dataType"="integer", "required"=true, "description"="Id клиента"},
     *     {"name"="collection", "dataType"="collection", "required"=true, "description"="Id клиента"}
     * },
     * statusCodes={
     *         200="Успех",
     *         404="Не найден объект"
     *     },
     * output="WebAnt\UserBundle\Entity\Client"
     * )
     */
    public function getClientAction($id)
    {
        return parent::getObject($id);
    }

    /**
     * @ApiDoc(
     * description="Получение списка клиентов",
     * section = "Clients",
     * filters={
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="start", "dataType"="integer"},
     *         {"name"="orderby", "dataType"="string"},
     *         {"name"="orderbydesc", "dataType"="string"}
     * },
     * statusCodes={
     *         200="Успех",
     *         404="Не найдены объекты"
     *     }
     * )
     */
    public function getClientsAction(Request $request)
    {
        return parent::getListObject($request);
    }

    /**
     * @ApiDoc(
     * description="Добавление клиента",
     * section = "Clients",
     * requirements = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Имя клиента"},
     *     {"name"="allowed_grant_types", "dataType"="array", "required"=true, "description"="Доступные методы"},
     * },
     * statusCodes={
     *         200="Успех",
     *         404="Не найдены объекты"
     *     }
     * )
     */
    public function postClientsAction(Request $request)
    {
        $data = $this->checkJson($request);

        $data['user_agent'] = $request->headers->get('user-agent');
        return parent::createObject($data);
    }

    /**
     * @ApiDoc(
     * description="Изменить клиента",
     * section = "Clients",
     * requirements = {
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Имя клиента"},
     *     {"name"="allowed_grant_types", "dataType"="array", "required"=true, "description"="Доступные методы"},
     * },
     * statusCodes={
     *         200="Успех",
     *         404="Не найдены объекты"
     *     }
     * )
     */
    public function putClientsAction(Request $request, $id)
    {
        $data = $this->checkJson($request);

        return parent::updateObject($data, $id);
    }


    /**
     * @ApiDoc(
     * description="Удаление клиента",
     * section = "User",
     * requirements = {
     *     {"name"="id", "dataType"="int", "required"=true, "description"="ID клиента"}
     * },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры",
     *         404="Не найден объект"
     * }
     * )
     */
    public function deleteClientAction($id)
    {
        return parent::deleteObject($id);
    }
} 