<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\TagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\Common\Collections\ArrayCollection;
use WebAnt\PassBundle\Entity\Pass;


/**
 * @ORM\Entity
 * @ORM\Table()
 * @UniqueEntity("name")
 */
class Tag
{

    public function __construct()
    {
        $this->dateCreate = new \DateTime();
        $this->folder = new ArrayCollection();
        $this->pass = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"getTag"})
     */
    protected $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     * @Groups({"getTag"})
     */
    protected $name;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\PassBundle\Entity\Pass")
     */
    private $pass;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="WebAnt\PassBundle\Entity\Folder")
     */
    private $folder;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @param Pass $tag
     * @return $this
     */
    public function addPass(Pass $pass)
    {
        if (!$this->pass->contains($pass)) {
            $this->pass->add($pass);
        }
        return $this;
    }


    /**
     * @param Tag $tag
     * @return $this
     */
    public function removePass(Pass $pass)
    {
        if ($this->pass->contains($pass)) {
            $this->pass->removeElement($pass);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param mixed $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }


}