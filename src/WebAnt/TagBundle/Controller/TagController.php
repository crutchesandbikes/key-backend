<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\TagBundle\Controller;


use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class TagController extends AbstractController
{
    public function __construct()
    {
        $this->objectClass = 'WebAnt\TagBundle\Entity\Tag';
        $this->objectKey   = 'id';
    }

    /**
     * @ApiDoc(
     * description="Создание Tag",
     * section = "Tag",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Tag"},
     *     {"name"="description", "dataType"="string", "required"=true, "description"="Описание Tag"},
     * },
     * output={
     *       "class"="WebAnt\TagBundle\Entity\Tag",
     *       "groups"={"getTag"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
    */
    public function postTagAction(Request $request)
    {
        $data = $this->checkJson($request);

        return parent::getObjectGroup(parent::createObject($data), 'getTag');
    }

    /**
    * @ApiDoc(
    * description="Изменение Tag",
    * section = "Tag",
    * requirements = {
    *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Tag"}
    * },
    * parameters = {
    *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Tag"},
    *     {"name"="description", "dataType"="string", "required"=true, "description"="Описание Tag"},
    * },
    * output={
    *       "class"="WebAnt\TagBundle\Entity\Tag",
    *       "groups"={"getTag"}
    *     },
    * statusCodes={
    *         200="Успех",
    *         400="Не все параметры",
    *         404="Не найден объект"
    *     }
    * )
    */
    public function putTagAction(Request $request, $id)
    {
        $data = $this->checkJson($request);

        return parent::getObjectGroup(parent::updateObject($data, $id), 'getTag');
    }

    /**
    * @ApiDoc(
    * description="Получение списка Tagss",
    * section = "Tag",
    * filters={
    *         {"name"="limit", "dataType"="integer"},
    *         {"name"="start", "dataType"="integer"},
    *         {"name"="orderby", "dataType"="string"},
    *         {"name"="orderbydesc", "dataType"="string"}
    * },
    * output={
    *       "class"="WebAnt\TagBundle\Entity\Tag",
    *       "groups"={"getTag"}
    *     },
    * statusCodes={
    *         200="Успех",
    *     }
    * )
    */
    public function getTagsAction(Request $request)
    {
        return parent::getObjectGroup(parent::getListObject($request), 'getTag');
    }

    /**
    * @ApiDoc(
    * description="Получение информации о Tag",
    * section = "Tag",
    * requirements = {
    *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Tag"}
    * },
    * output={
    *       "class"="WebAnt\TagBundle\Entity\Tag",
    *       "groups"={"getTag"}
    *     },
    * statusCodes={
    *         200="Успех",
    *         404="Объект не найден"
    *     }
    * )
    */
    public function getTagAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), 'getTag');
    }

    /**
    * @ApiDoc(
    * description="Удаление Tag",
    * section = "Tag",
    * requirements = {
    *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Tag"}
    * },
    * output={
    *       "class"="WebAnt\TagBundle\Entity\Tag",
    *       "groups"={"getTag"}
    *     },
    * statusCodes={
    *         200="Успех",
    *         400="Не все параметры",
    *         404="Не найден объект"
    * }
    * )
    */
    public function deleteTagAction($id)
    {
        return parent::deleteObject($id);
    }
}
