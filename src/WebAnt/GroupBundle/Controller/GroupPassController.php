<?php

/**
 * This file is part of the WebAnt Skeleton package.
 *
 * LTD WebAnt <support@webant.ru>
 * Developer Yuri Kovalev <u@webant.ru>
 *
 */

namespace WebAnt\GroupBundle\Controller;


use WebAnt\CoreBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class GroupPassController extends AbstractController
{
    public function __construct()
    {
        $this->objectClass = 'WebAnt\GroupBundle\Entity\GroupPass';
        $this->objectKey   = 'id';
    }

    /**
     * @ApiDoc(
     * description="Создание Group",
     * section = "Group",
     * parameters={
     *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Group"},
     *     {"name"="description", "dataType"="string", "required"=true, "description"="Описание Group"},
     * },
     * output={
     *       "class"="WebAnt\GroupBundle\Entity\GroupPass",
     *       "groups"={"getGroup"}
     *     },
     * statusCodes={
     *         200="Успех",
     *         400="Не все параметры"
     *     }
     * )
    */
    public function postGroupAction(Request $request)
    {
        $data = $this->checkJson($request);

        return parent::getObjectGroup(parent::createObject($data), 'getGroup');
    }

    /**
    * @ApiDoc(
    * description="Изменение Group",
    * section = "Group",
    * requirements = {
    *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Group"}
    * },
    * parameters = {
    *     {"name"="name", "dataType"="string", "required"=true, "description"="Название Group"},
    *     {"name"="description", "dataType"="string", "required"=true, "description"="Описание Group"},
    * },
    * output={
    *       "class"="WebAnt\GroupBundle\Entity\GroupPass",
    *       "groups"={"getGroup"}
    *     },
    * statusCodes={
    *         200="Успех",
    *         400="Не все параметры",
    *         404="Не найден объект"
    *     }
    * )
    */
    public function putGroupAction(Request $request, $id)
    {
        $data = $this->checkJson($request);

        return parent::getObjectGroup(parent::updateObject($data, $id), 'getGroup');
    }

    /**
    * @ApiDoc(
    * description="Получение списка Groupss",
    * section = "Group",
    * filters={
    *         {"name"="limit", "dataType"="integer"},
    *         {"name"="start", "dataType"="integer"},
    *         {"name"="orderby", "dataType"="string"},
    *         {"name"="orderbydesc", "dataType"="string"}
    * },
    * output={
    *       "class"="WebAnt\GroupBundle\Entity\GroupPass",
    *       "groups"={"getGroup"}
    *     },
    * statusCodes={
    *         200="Успех",
    *     }
    * )
    */
    public function getGroupsAction(Request $request)
    {
        return parent::getObjectGroup(parent::getListObject($request), 'getGroup');
    }

    /**
    * @ApiDoc(
    * description="Получение информации о Group",
    * section = "Group",
    * requirements = {
    *     {"name"="id", "dataType"="integer", "required"=true, "description"="ID Group"}
    * },
    * output={
    *       "class"="WebAnt\GroupBundle\Entity\GroupPass",
    *       "groups"={"getGroup"}
    *     },
    * statusCodes={
    *         200="Успех",
    *         404="Объект не найден"
    *     }
    * )
    */
    public function getGroupAction($id)
    {
        return parent::getObjectGroup(parent::getObject($id), 'getGroup');
    }

    /**
    * @ApiDoc(
    * description="Удаление Group",
    * section = "Group",
    * requirements = {
    *     {"name"="id", "dataType"="int", "required"=true, "description"="ID Group"}
    * },
    * output={
    *       "class"="WebAnt\GroupBundle\Entity\GroupPass",
    *       "groups"={"getGroup"}
    *     },
    * statusCodes={
    *         200="Успех",
    *         400="Не все параметры",
    *         404="Не найден объект"
    * }
    * )
    */
    public function deleteGroupAction($id)
    {
        return parent::deleteObject($id);
    }
}
